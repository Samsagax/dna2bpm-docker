
Build
-----

`$ docker build -t "php-mongo-image" .`

Run
---

Just to run this container

`$ docker run -p 8080:80 -d -v <path_to_php_app>:/var/www/site php-mongo-image`

# Link this container with a Mongo-DB instance

- First run the MongoDB instance with a volume container

`$ docker run --name mongodb-instance -d -v /data/db mongo`

- Link the instance with our image

`$ docker run --name server-php -d -v <path_to_php_app>:/var/www/site php-mongo-image`

# Using Docker Composer

1) You must edit <path_to_php_app> in docker-compose.yml
2) `$ docker-compose build`
3) `$ docker-compose up`
